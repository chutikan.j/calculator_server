Routes list:
    - {host}/v1/calculator.sum
    - {host}/v1/calculator.mul
    - {host}/v1/calculator.div
    - {host}/v1/calculator.sub

Post request body in JSON format:
{
    "a": 100,
    "b": 200.2
}
