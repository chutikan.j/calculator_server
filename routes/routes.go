package routes

import (
	"cal_server/controllers"
	"net/http"

	"github.com/gorilla/mux"
)

type Route struct {
	Controller controllers.IController
}

func (r *Route) Routes() *mux.Router {
	route := mux.NewRouter()

	route.HandleFunc("/calculator.sum", r.Controller.Sum).Methods(http.MethodPost).Name("sum")
	route.HandleFunc("/calculator.mul", r.Controller.Mul).Methods(http.MethodPost).Name("mul")
	route.HandleFunc("/calculator.sub", r.Controller.Sub).Methods(http.MethodPost).Name("sub")
	route.HandleFunc("/calculator.div", r.Controller.Div).Methods(http.MethodPost).Name("div")

	return route
}
