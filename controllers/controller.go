package controllers

import (
	"cal_server/models"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net/http"
)

type ControllerStruct struct{}

func (c *ControllerStruct) Sum(w http.ResponseWriter, r *http.Request) {
	log.Println("action summation")
	req, err := c.readBody(r)
	if err != nil {
		c.failCase(w, err)
		return
	}

	result := req.A + req.B
	body := &models.DataItem{
		Operand:  fmt.Sprintf("A: %v, B: %v", req.A, req.B),
		Operator: r.URL.Path,
		Result:   math.Round(float64(result)),
	}
	c.successCase(w, body)
	return
}

func (c *ControllerStruct) Mul(w http.ResponseWriter, r *http.Request) {
	log.Println("action multiple")

	req, err := c.readBody(r)
	if err != nil {
		c.failCase(w, err)
		return
	}
	result := req.A * req.B
	body := &models.DataItem{
		Operand:  fmt.Sprintf("A: %v, B: %v", req.A, req.B),
		Operator: r.URL.Path,
		Result:   math.Round(float64(result)),
	}
	c.successCase(w, body)
}

func (c *ControllerStruct) Sub(w http.ResponseWriter, r *http.Request) {
	log.Println("action subtraction")

	req, err := c.readBody(r)
	if err != nil {
		c.failCase(w, err)
		return
	}

	result := req.A - req.B
	body := &models.DataItem{
		Operand:  fmt.Sprintf("A: %v, B: %v", req.A, req.B),
		Operator: r.URL.Path,
		Result:   math.Round(float64(result)),
	}
	c.successCase(w, body)
}

func (c *ControllerStruct) Div(w http.ResponseWriter, r *http.Request) {
	log.Println("action division")
	req, err := c.readBody(r)
	if err != nil {
		c.failCase(w, err)
		return
	}

	if req.B == 0 {
		c.failCase(w, errors.New("divisor(B) should not be zero"))
		return
	}

	var result = req.A / req.B
	body := &models.DataItem{
		Operand:  fmt.Sprintf("A: %v, B: %v", req.A, req.B),
		Operator: r.URL.Path,
		Result:   math.Round(float64(result * 100 / 100)),
	}
	c.successCase(w, body)
}

func (c *ControllerStruct) failCase(w http.ResponseWriter, err error) {
	data := &models.FailCase{
		StatusText:   http.StatusText(http.StatusBadRequest),
		ErrorMessage: err.Error(),
	}
	strData, err := json.Marshal(data)
	if err != nil {
		log.Print("unmarshal data fail")
	}
	w.Header().Add("content-type", "application/json")
	w.WriteHeader(http.StatusBadRequest)
	_, err = w.Write(strData)
	if err != nil {
		log.Print("writing byte fail")
	}
}

func (c *ControllerStruct) successCase(w http.ResponseWriter, body interface{}) {
	data := &models.SuccessCase{
		StatusText: http.StatusText(http.StatusOK),
		Data:       body,
	}
	strData, err := json.Marshal(data)
	if err != nil {
		log.Print("unmarshal data fail")
	}
	w.Header().Add("content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, err = w.Write(strData)
	if err != nil {
		log.Print("writing byte fail")
	}
}

func (c *ControllerStruct) readBody(r *http.Request) (*models.RequestBody, error) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Print("fail to read request body")
	}
	objBody := new(models.RequestBody)
	err = json.Unmarshal(body, objBody)
	if err != nil {
		return nil, fmt.Errorf("error while unmarshal data (%v)", err)
	}

	return objBody, nil
}
