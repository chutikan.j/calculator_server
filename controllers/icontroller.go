package controllers

import "net/http"

type IController interface {
	Sum(w http.ResponseWriter, r *http.Request)
	Mul(w http.ResponseWriter, r *http.Request)
	Sub(w http.ResponseWriter, r *http.Request)
	Div(w http.ResponseWriter, r *http.Request)
}
