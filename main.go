package main

import (
	"cal_server/controllers"
	"cal_server/routes"
	"context"
	"os/signal"

	// "cal_server/services"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/joho/godotenv"
)

func main() {
	var wait time.Duration
	wait = time.Second * 20
	err := godotenv.Load()
	if err != nil {
		log.Fatal("error loading .env file")
	}

	cont := &controllers.ControllerStruct{}

	route := routes.Route{Controller: cont}
	srv := &http.Server{
		Handler: route.Routes(),
		Addr:    os.Getenv("CALCULATOR_PORT"),
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	go func() {
		log.Println("server is running!")
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()

	_ = srv.Shutdown(ctx)
	log.Println("shutting down")
	os.Exit(0)

}
