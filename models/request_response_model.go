package models

type FailCase struct {
	StatusText   string `json:"status_text"`
	ErrorMessage string `json:"error_message"`
}

type SuccessCase struct {
	StatusText string      `json:"status_text"`
	Data       interface{} `json:"data"`
}

type DataItem struct {
	Operand  string  `json:"operand"`
	Operator string  `json:"operator"`
	Result   float64 `json:"result"`
}

type RequestBody struct {
	A float32 `json:"a"`
	B float32 `json:"b"`
}
